//
//  OMCMainViewController.h
//  OgrencininMutfagi
//
//  Created by Samet Gültekin on 04/12/13.
//  Copyright (c) 2013 Samet Gültekin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WPCategory;
@interface OMCMainViewController : UIViewController
@property (nonatomic, strong) WPCategory *activeCategory;
@end
