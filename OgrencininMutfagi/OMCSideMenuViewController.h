//
//  OMCSideMenuViewController.h
//  OgrencininMutfagi
//
//  Created by Samet Gültekin on 04/12/13.
//  Copyright (c) 2013 Samet Gültekin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OMCMainViewController;
@class MSDynamicsDrawerViewController;
@interface OMCSideMenuViewController : UIViewController
@property (nonatomic, weak) OMCMainViewController *mainViewController;
@property (nonatomic, weak) MSDynamicsDrawerViewController *dynamicsDrawer;
@end
