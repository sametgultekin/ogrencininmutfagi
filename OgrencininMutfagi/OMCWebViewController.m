//
//  OMCWebViewController.m
//  OgrencininMutfagi
//
//  Created by Samet Gültekin on 25/12/13.
//  Copyright (c) 2013 Samet Gültekin. All rights reserved.
//

#import "OMCWebViewController.h"
#import "UIImageView+AFNetworking.h"
#import "FXBlurView.h"

@interface OMCWebViewController ()
@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSMutableString *htmlString;
@property (nonatomic, strong) UIImageView *backgroundImage;
@property (nonatomic, strong) FXBlurView *blurView;
@end

@implementation OMCWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = self.recipeHeader.title;
    [self.webView loadHTMLString:_htmlString baseURL:[NSURL URLWithString:@"http://ogrencininmutfagi.com"]];
    
    __block UIView *rootView = self.view;
    self.backgroundImage = [[UIImageView alloc] init];
    __block UIImageView *bgImage = self.backgroundImage;

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"menu-background"]]];
//    NSLog(@"asd:%@", self.recipeHeader.thumbnailImages.fullSizeImage.url);
//    [self.backgroundImage setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.recipeHeader.thumbnailImages.fullSizeImage.url]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
//        [bgImage setImage:image];
//        
//        FXBlurView *blrView = [[FXBlurView alloc] initWithFrame:rootView.frame];
//        [blrView addSubview:bgImage];
//        [rootView insertSubview:blrView atIndex:0];
//
//        
//    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//        
//    }];
    
//    NSString *regex = @"\"([^\"]*)\"";
//    NSRegularExpression *nameExpression = [NSRegularExpression regularExpressionWithPattern:regex options:0 error:nil];
//    
//    NSDictionary *dict = self.recipeHeader.customFields;
//    NSString *text = [dict[@"RECIPE_META_ingredients"] firstObject];
//    NSLog(@"str:%@", text);
//    NSArray *matches = [nameExpression matchesInString:text
//                                               options:0
//                                                 range:NSMakeRange(0, [text length])];
//    for (NSTextCheckingResult *match in matches) {
//        //NSRange matchRange = [match range];
//        NSRange matchRange = [match rangeAtIndex:1];
//        NSString *matchString = [text substringWithRange:matchRange];
//        NSLog(@"%@", matchString);
//    }

    
    

}


- (void)setRecipeHeader:(WPPost *)recipeHeader{
    
    _recipeHeader = recipeHeader;
    _htmlString = [[NSMutableString alloc] init];
    [_htmlString appendString:@"<html><head><meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\"> <meta name=\"apple-mobile-web-app-capable\" content=\"yes\"></head> <style>body{color:white; font-family: HelveticaNeue-Light; width:100%;} img{max-width:100%; width:100%; padding-left:0px; margin-left:0; margin: auto;} #container{max-width:96%; }      </style><body><div id=\"container\">"];
    
    [_htmlString appendString:_recipeHeader.content];
    [_htmlString appendString:@"</div></body></html>"];
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
