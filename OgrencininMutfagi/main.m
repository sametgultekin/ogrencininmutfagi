//
//  main.m
//  OgrencininMutfagi
//
//  Created by Samet Gültekin on 04/12/13.
//  Copyright (c) 2013 Samet Gültekin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OMCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([OMCAppDelegate class]));
    }
}
