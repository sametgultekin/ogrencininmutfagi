//
//  OMCSideMenuViewController.m
//  OgrencininMutfagi
//
//  Created by Samet Gültekin on 04/12/13.
//  Copyright (c) 2013 Samet Gültekin. All rights reserved.
//

#import "OMCSideMenuViewController.h"
#import "MSDynamicsDrawerStyler.h"
#import "OMCMainViewController.h"
#import "WPWrapper.h"

@interface OMCSideMenuViewController () <UITableViewDataSource, UITableViewDelegate, MSDynamicsDrawerStyler>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *categoryArray;
@property (nonatomic, getter = isLoading) BOOL loading;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@end

@implementation OMCSideMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mainViewRegistered:) name:@"registerMainView" object:nil];

    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl setTintColor:[UIColor  whiteColor]];
    [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.frame];
    [backgroundView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"darkbg"]]];
    [self.tableView setBackgroundView:backgroundView];
    self.tableView.separatorColor = [UIColor colorWithWhite:1.0 alpha:0.25];

    
    [self refresh];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)mainViewRegistered:(id)sender{
    
    NSLog(@"object:%@", [sender description]);
    self.mainViewController = (OMCMainViewController*)sender;
}


#pragma mark - TableView Delegate - DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.categoryArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    WPCategory *category = self.categoryArray[indexPath.row];
    cell.textLabel.text = category.title;
    return cell;
    
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    UIView *selectedBackgroundView = [UIView new];
    selectedBackgroundView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.25];
    cell.selectedBackgroundView = selectedBackgroundView;
}
- (void)setLoading:(BOOL)loading{
    
    if (loading == NO) {
        [self.refreshControl endRefreshing];
    }
    else{
        [self.refreshControl beginRefreshing];
    }
    
    _loading = loading;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    WPCategory *selectedCategory = self.categoryArray[indexPath.row];
    [self.mainViewController setActiveCategory:selectedCategory];
    [self.dynamicsDrawer setPaneState:MSDynamicsDrawerPaneStateClosed animated:YES allowUserInterruption:YES completion:nil];
    
}


- (void)refresh{
    
    self.loading = YES;
    WPIndexFetcher *fetcher = [[WPIndexFetcher alloc] init];
    [fetcher fetchCategoryIndexWithCompletion:^(WPCategoryIndexResponse *response, NSError *error) {
        self.loading = NO;
        if (!error) {
            self.categoryArray = response.categories;
            [self.tableView reloadData];
        }
        else{
            NSLog(@"error:%@", error);
        }
    }];
    
}

#pragma  mark - MSDynamicsDrawer

- (void)dynamicsDrawerViewController:(MSDynamicsDrawerViewController *)dynamicsDrawerViewController didUpdatePaneClosedFraction:(CGFloat)paneClosedFraction forDirection:(MSDynamicsDrawerDirection)direction{
    
}


@end
