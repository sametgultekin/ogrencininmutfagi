//
//  OMCRecipeViewController.m
//  OgrencininMutfagi
//
//  Created by Samet Gültekin on 06/12/13.
//  Copyright (c) 2013 Samet Gültekin. All rights reserved.
//

#import "OMCRecipeViewController.h"
#import "UIImageView+AFNetworking.h"

@interface OMCRecipeViewController () <UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) IBOutlet UICollectionViewController *collectionView;
@property (nonatomic, getter = isLoading) BOOL loading;
@property (nonatomic, strong) WPPost *recipe;
@end

@implementation OMCRecipeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.collectionView.view setBackgroundColor:[UIColor redColor]];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)setRecipeHeader:(WPPost *)recipeHeader{
    _recipeHeader = recipeHeader;
    [self refresh];
}

- (void)refresh{
    
    self.loading = YES;
    WPPostFetcher *fetcher = [[WPPostFetcher alloc] init];
    [fetcher fetchPostWithPostID:self.recipeHeader.postID options:@{@"post_type": @"recipe"} completion:^(WPPost *post, NSError *error) {
        self.loading = NO;
        if (!error) {
            self.recipe = post;
            NSLog(@"recipe: %@", [self.recipe dictionaryRepresentation]);
        }
        
    }];
    
}

#pragma mark - UICollectionView Datasource
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return 1;
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 10;
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"RecipeCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    return cell;
}
// 4
/*- (UICollectionReusableView *)collectionView:
 (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 return [[UICollectionReusableView alloc] init];
 }*/

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(self.view.frame.size.width - 40, self.view.frame.size.height - 200);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(50, 20, 50, 20);
}

@end
