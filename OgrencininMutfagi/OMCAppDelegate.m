//
//  OMCAppDelegate.m
//  OgrencininMutfagi
//
//  Created by Samet Gültekin on 04/12/13.
//  Copyright (c) 2013 Samet Gültekin. All rights reserved.
//

#import "OMCAppDelegate.h"
#import "MSDynamicsDrawerViewController.h"
#import "OMCMainViewController.h"
#import "OMCSideMenuViewController.h"
#import "WPWrapper.h"

@implementation OMCAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [WPClient instanceWithOptions:@{@"baseURL" : @"http://www.ogrencininmutfagi.com/api/"}];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.dynamicsDrawerViewController = (MSDynamicsDrawerViewController *)self.window.rootViewController;
    
     OMCSideMenuViewController *menuViewController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    [menuViewController setDynamicsDrawer:self.dynamicsDrawerViewController];
    
    [self.dynamicsDrawerViewController setDrawerViewController:menuViewController forDirection:MSDynamicsDrawerDirectionLeft];
    
    [self.dynamicsDrawerViewController.view setBackgroundColor:[UIColor clearColor]];
    
    
    UINavigationController *navController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"MainNavigation"];
    [self.dynamicsDrawerViewController setPaneViewController:navController animated:NO completion:nil];
    [menuViewController setMainViewController:navController.viewControllers[0]];
    
    
    [self.window setRootViewController:self.dynamicsDrawerViewController];
    [self.window makeKeyAndVisible];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
