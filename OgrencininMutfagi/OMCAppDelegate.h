//
//  OMCAppDelegate.h
//  OgrencininMutfagi
//
//  Created by Samet Gültekin on 04/12/13.
//  Copyright (c) 2013 Samet Gültekin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MSDynamicsDrawerViewController;
@interface OMCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MSDynamicsDrawerViewController *dynamicsDrawerViewController;
@end
