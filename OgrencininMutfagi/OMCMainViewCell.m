//
//  OMCMainViewCell.m
//  OgrencininMutfagi
//
//  Created by Samet Gültekin on 05/12/13.
//  Copyright (c) 2013 Samet Gültekin. All rights reserved.
//

#import "OMCMainViewCell.h"
#import "UIImageView+AFNetworking.h"

@implementation OMCMainViewCell

- (void)awakeFromNib{
    
    [self.contentView setBackgroundColor:[UIColor clearColor]];
    [self setBackgroundColor:[UIColor clearColor]];
    [super awakeFromNib];
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
