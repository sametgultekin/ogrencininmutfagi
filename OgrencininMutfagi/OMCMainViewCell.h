//
//  OMCMainViewCell.h
//  OgrencininMutfagi
//
//  Created by Samet Gültekin on 05/12/13.
//  Copyright (c) 2013 Samet Gültekin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXBlurView.h"
@interface OMCMainViewCell : UITableViewCell
@property (nonatomic, strong) IBOutlet FXBlurView *recipeContainerView;
@property (nonatomic, strong) IBOutlet UILabel *recipeTitleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *recipeImageView;
@end
