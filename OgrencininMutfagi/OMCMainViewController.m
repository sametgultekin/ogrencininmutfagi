//
//  OMCMainViewController.m
//  OgrencininMutfagi
//
//  Created by Samet Gültekin on 04/12/13.
//  Copyright (c) 2013 Samet Gültekin. All rights reserved.
//

#import "OMCMainViewController.h"
#import "WPWrapper.h"
#import "OMCMainViewCell.h"
#import "OMCRecipeViewController.h"
#import "UIImageView+AFNetworking.h"
#import "OMCAppDelegate.h"
#import "MSDynamicsDrawerViewController.h"
#import "OMCWebViewController.h"

@interface OMCMainViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, 	strong) UITableViewController *tableViewController;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray* recipeArray;
@property (nonatomic, getter = isLoading) BOOL loading;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) MSDynamicsDrawerViewController *dynamicsDrawer;
@end

@implementation OMCMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"registerMainView" object:self];

    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.frame];
    [backgroundView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"menu-background"]]];
    [self.tableView setBackgroundView:backgroundView];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl setTintColor:[UIColor whiteColor]];
    self.title = @"Öğrencinin Mutfağı";
    

    
    [self refresh];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setActiveCategory:(WPCategory *)activeCategory{
    
    _activeCategory = activeCategory;
    
    if (activeCategory == nil) {
    }
    else{
        
        self.recipeArray = nil;
    }
    
    [self refresh];
    
}


- (void)setLoading:(BOOL)loading{
    
    if (loading) {
        [self.tableViewController.refreshControl beginRefreshing];
    }
    else{
        [self.tableViewController.refreshControl endRefreshing];
    }
    _loading = loading;
}


- (void)refresh{
    
    if (!self.activeCategory) {
        [self refreshRecents];
        return;
    }
    
    
    self.loading = YES;
    
    WPPostFetcher *fetcher = [[WPPostFetcher alloc] init];
    

    [fetcher fetchPostsByCategoryID:self.activeCategory.categoryID options:@{@"post_type": @"recipe"} completion:^(WPPostsByCategoryResponse *response, NSError *error) {
        self.loading = NO;
        if(!error){
            self.recipeArray = response.posts;
            [self.tableView reloadData];
        }
        
    }];
    


}


- (void)refreshRecents{
    self.loading = YES;
    WPPostFetcher *fetcher = [[WPPostFetcher alloc] init];
    [fetcher fetchRecentPostsWithOptions:@{@"post_type": @"recipe"} completion:^(WPRecentPostsResponse *response, NSError *error) {
        self.loading = NO;
        if (!error) {
            self.recipeArray = response.posts;
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - TableView

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.recipeArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OMCMainViewCell *cell = (OMCMainViewCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    WPPost *post = self.recipeArray[indexPath.row];
    cell.recipeTitleLabel.text = post.titlePlain;
    
    
    NSURL *imageURL;
    if (post.thumbnailImages.fullSizeImage) {
        imageURL = [NSURL URLWithString:post.thumbnailImages.fullSizeImage.url];
    }
    else if (post.thumbnailImages.mediumSizeImage){
        imageURL = [NSURL URLWithString:post.thumbnailImages.mediumSizeImage.url];
    }
    else if (post.thumbnailImages.thumbnailSizeImage){
        imageURL = [NSURL URLWithString:post.thumbnailImages.thumbnailSizeImage.url];
    }
    else if (post.thumbnailImages.postThumbnailSizeImage){
        imageURL = [NSURL URLWithString:post.thumbnailImages.postThumbnailSizeImage.url];
    }
    
    [cell.recipeImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder"]];
    

    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma - segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"ShowRecipe"]) {
        
        WPPost *recipe = [self.recipeArray objectAtIndex:self.tableView.indexPathForSelectedRow.row];
//        OMCRecipeViewController *recipeViewController = (OMCRecipeViewController*)[segue destinationViewController];
//        [recipeViewController setRecipeHeader:recipe];
        OMCWebViewController *webView = (OMCWebViewController*)[segue destinationViewController];
        [webView setRecipeHeader:recipe];
        
    }
}


@end
