//
//  OMCRecipeViewController.h
//  OgrencininMutfagi
//
//  Created by Samet Gültekin on 06/12/13.
//  Copyright (c) 2013 Samet Gültekin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WPWrapper.h"

@interface OMCRecipeViewController : UIViewController
@property (nonatomic, strong) WPPost *recipeHeader;
@end
